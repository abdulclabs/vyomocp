//
//  ServiceTableViewCell.swift
//  VyomoCP
//
//  Created by Click Labs on 4/16/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var cellBackgroundImage: UIImageView!
    @IBOutlet weak var salonNameCellLabel: UILabel!
    @IBOutlet weak var addressCellLabel: UILabel!
    @IBOutlet weak var costCellLabel: UILabel!
    @IBOutlet weak var distanceCellLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
