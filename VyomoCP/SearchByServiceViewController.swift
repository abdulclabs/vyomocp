//
//  SearchByServiceViewController.swift
//  VyomoCP
//
//  Created by Click Labs on 4/15/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class SearchByServiceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,UIScrollViewDelegate {

    //MARK : Services views on scroll views...
    @IBOutlet weak var serviceOne_OnScrollOne: UIView!
    var selectedServiceCounter = 0
    var data = ["Abdul", "Danish", "Zeeshan", "Akbar"]
    var buisenessId = [Int]()
    var filtered:[String] = []
    var searchString = [String]()
    var oldData = ["Abdul", "Abu","A","Danish", "Zeeshan", "Akbar"]
    var searchCounter = 0
    var userSelection = String()
    var imageCache = [String : UIImage]()
    var imageUrls = [String]()
    var imageArray = [UIImage]()
    
//    var arr = ["Abdul", "Danish", "Zeeshan", "Akbar"]
//    var rupee = ["60", "70", "80", "90"]
  
    @IBOutlet weak var searchSalonTextField: UITextField!
    
    @IBOutlet weak var byServicesButton: UIButton!
    @IBOutlet weak var bySalonButton: UIButton!
    @IBOutlet weak var locationPointer: UILabel!
    @IBOutlet weak var searchByNamePointer: UILabel!
    @IBOutlet weak var shoppingCartButton: UIButton!
    @IBOutlet weak var numberOfSelectedServicesLabel: UILabel!
    @IBOutlet weak var foundServicesView: UIView!
    @IBOutlet weak var salonTableView: UITableView!
    @IBOutlet weak var verticalScrollView: UIScrollView!
    
    
    @IBOutlet weak var servicesHorizontalScrollOne: UIScrollView!
    @IBOutlet weak var servicesHorizontalScrollTwo: UIScrollView!
    @IBOutlet weak var servicesHorizontalScrollThree: UIScrollView!
    
    @IBOutlet weak var searchServicesView: UIView!
    @IBOutlet weak var rupeeLabel: UILabel!
    
    @IBAction func byServicesButton(sender: AnyObject) {
        verticalScrollView.hidden = false
        shoppingCartButton.hidden = false
        numberOfSelectedServicesLabel.hidden = false
        foundServicesView.hidden = true
        salonTableView.hidden = true
        searchServicesView.hidden = true
        byServicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        bySalonButton.backgroundColor = UIColor.clearColor()
    }
    @IBAction func bySalonButton(sender: UIButton) {
        verticalScrollView.hidden = true
        foundServicesView.hidden = false
        salonTableView.hidden = false
        shoppingCartButton.hidden = true
        numberOfSelectedServicesLabel.hidden = true
        searchServicesView.hidden = false
        bySalonButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        byServicesButton.backgroundColor = UIColor.clearColor()
    }
    
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var viewThreeHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTwoHeight: NSLayoutConstraint!
    
    
     //MARK: - Services buttons...
    @IBAction func doorStepButton(sender: UIButton) {
        
        println("Shamim Khan")
        servicesHorizontalScrollOne.hidden = false
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 178
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 110
    }
    
    @IBAction func hairButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = false
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 178
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 110
    }
    
    @IBAction func faceCareButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = false
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 178
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 110
    }
    
    @IBAction func handsfeetButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = false
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 110
        viewTwoHeight.constant = 178
        viewThreeHeight.constant = 110
    }
    
    @IBAction func bridalButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = false
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 110
        viewTwoHeight.constant = 178
        viewThreeHeight.constant = 110
        
    }
    
    @IBAction func spa_massageButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = false
        servicesHorizontalScrollThree.hidden = true
        heightView.constant = 110
        viewTwoHeight.constant = 178
        viewThreeHeight.constant = 110
    }
    
    @IBAction func men_salonButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = false
        heightView.constant = 110
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 178
    }
    
    @IBAction func clinicButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = false
        heightView.constant = 110
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 178
    }
    
    @IBAction func moreButton(sender: UIButton) {
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = false
        heightView.constant = 110
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 178
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heightView.constant = 110
        viewTwoHeight.constant = 110
        viewThreeHeight.constant = 110
        servicesHorizontalScrollOne.hidden = true
        servicesHorizontalScrollTwo.hidden = true
        servicesHorizontalScrollThree.hidden = true
        searchSalonTextField.tintColor = UIColor.whiteColor()
        shoppingCartButton.hidden = false
        numberOfSelectedServicesLabel.hidden = false
        verticalScrollView.hidden = false
        numberOfSelectedServicesLabel.layer.cornerRadius = numberOfSelectedServicesLabel.frame.size.width/2
        numberOfSelectedServicesLabel.layer.masksToBounds = true
        
        locationPointer.layer.cornerRadius = locationPointer.frame.size.width/2
        locationPointer.layer.masksToBounds = true
        
        searchByNamePointer.layer.cornerRadius = searchByNamePointer.frame.size.width/2
        searchByNamePointer.layer.masksToBounds = true
        searchServicesView.hidden = true
        byServicesButton.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        self.salonTableView.rowHeight = 130
        
        
        var tapRecognizer = UITapGestureRecognizer(target: self, action: "scrollViewTapped")
        
            serviceOne_OnScrollOne.addGestureRecognizer(tapRecognizer)
            }

    func scrollViewTapped() {
       println("lol")
    }
    
    func serviceCounter(){
        selectedServiceCounter++
        numberOfSelectedServicesLabel.text = "\(selectedServiceCounter)"
    }

    //MARK : Table Implementation..
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as ServiceTableViewCell
        
        cell.salonNameCellLabel.text = filtered[indexPath.row]
         return cell
    }

       
     //MARK : TextField Action
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if string != "" {                 //if element is typed not deleted
            searchString.append(string)
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter++
            println(modifiedString)
            println(searchString)
            search(modifiedString)
        } else {
            searchString.removeLast()
            var modifiedString = String()
            
            for i in searchString {
                modifiedString += i
            }
            searchCounter--
            println(searchString)
            println(modifiedString)
            search(modifiedString)
            
        }
        return true
    }

    // MARK: Search Implementation
    
    func search(string: String) {
        filtered = oldData.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString("\(string)", options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        if(filtered == []) && searchCounter == 0 {
            data = oldData
        } else {
            data = filtered
        }
        
        sort(&filtered)
        println(filtered)
        salonTableView.reloadData()
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
